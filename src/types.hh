/* types.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */

#ifndef __CHARLES_TYPES_HH__
#define __CHARLES_TYPES_HH__

#include <vector>


namespace charles {

/* Basic types */
typedef double Double;

typedef std::vector<Double> DoubleVector;
typedef DoubleVector TVector;

} /* namespace charles */

#endif /* __CHARLES_TYPES_HH__ */

