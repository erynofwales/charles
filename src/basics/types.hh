/* types.hh
 * vim: set tw=80:
 * Eryn Wells <eryn@erynwells.me>
 */
/**
 * Some basic types.
 */

#ifndef __BASICS_TYPES_HH__
#define __BASICS_TYPES_HH__

#include <vector>


typedef double Double;
typedef unsigned int UInt;

typedef std::vector<Double> DoubleVector;
typedef DoubleVector TVector;

#endif /* __BASICS_TYPES_HH__ */
